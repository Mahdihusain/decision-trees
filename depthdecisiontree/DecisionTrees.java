import java.util.Scanner;
import java.io.File;
import java.util.Vector;
import java.util.Arrays;

public class DecisionTrees{
	
	public class Node{
		int value;
		double median;
		Node left_child;
		Node right_child;
		
		public Node(int val, double med, Node lc, Node rc){
			
			value = val;
			median = med;
			left_child = lc;
			right_child = rc;
			
		}
		
	}
	
	public static class tuple{
		double train_acc;
		double valid_acc;
		double test_acc;
		
		public tuple(double tr, double va, double te){
			train_acc=tr;
			valid_acc=va;
			test_acc=te;
		}
		
	}
	
	public static String[] attributes;
	public static boolean[] is_discrete;
	
	public static void main(String[] args){
		
		attributes = new String[55];
		is_discrete = new boolean[54];
		Vector<Vector<Integer>> train_data = new Vector<Vector<Integer>>();
		Vector<Vector<Integer>> test_data = new Vector<Vector<Integer>>();
		Vector<Vector<Integer>> valid_data = new Vector<Vector<Integer>>();
		
		Vector<tuple> accs = new Vector<tuple>();
		
		DecisionTrees dt_obj = new DecisionTrees();
		
		try{
			
			Scanner scan_obj = new Scanner(new File("train.dat"));
			
			boolean isAttributes = true;
			
			while(scan_obj.hasNextLine()){
				
				String line = scan_obj.nextLine();
				
				if(isAttributes){
					isAttributes = false;
					attributes = line.split(",");
					
					for(int i=0; i<attributes.length-1; i++){
						
						if(attributes[i].contains("Discrete")){
							//System.out.println("hello");
							is_discrete[i] = true;
						}else{
							//System.out.println("hi");
							is_discrete[i] = false;
						}
						
					}
					
				}else{
					String[] td_str = line.split(",");
		            Vector<Integer> row = new Vector<Integer>(td_str.length);
					
					
					for(int i=0; i<td_str.length; i++){
						row.add(Integer.parseInt(td_str[i]));
						
					}
					
					train_data.add(row);

				}
				
			}
			
			scan_obj = new Scanner(new File("valid.dat"));
			isAttributes = true;
			
			while(scan_obj.hasNextLine()){
				
				String line = scan_obj.nextLine();
				
				if(isAttributes){
					isAttributes = false;
				}else{
					String[] td_str = line.split(",");
					Vector<Integer> row = new Vector<Integer>(td_str.length);
					
					
					for(int i=0; i<td_str.length; i++){
						row.add(Integer.parseInt(td_str[i]));
						
					}
					
					valid_data.add(row);

				}
				
			}
			
			scan_obj = new Scanner(new File("test.dat"));
			isAttributes = true;
			
			while(scan_obj.hasNextLine()){
				
				String line = scan_obj.nextLine();
				
				if(isAttributes){
					isAttributes = false;
				}else{
					String[] td_str = line.split(",");
					Vector<Integer> row = new Vector<Integer>(td_str.length);
					
					
					for(int i=0; i<td_str.length; i++){
						row.add(Integer.parseInt(td_str[i]));
						
					}
					
					test_data.add(row);

				}
				
			}
			
			System.out.println("import successful");
			
			for(int depth=0; depth<30; depth++){
				
				boolean allowed[] = new boolean[54];
			
				for(int i=0; i<54; i++){
					allowed[i]=true;
				}
				
				Node root = dt_obj.grow_tree(train_data, depth, allowed);
				System.out.println("tree successfully for depth " + depth);
				
				
				double train_acc = dt_obj.predict(train_data, root);
				
				System.out.println("traindata " + train_acc);
				
				//valid data

				double valid_acc = dt_obj.predict(valid_data, root);
				
				System.out.println("validdata " + valid_acc);
				
				//test data

				double test_acc = dt_obj.predict(test_data, root);
				
				System.out.println("testdata " + test_acc);
				
				tuple t_acc = new tuple(train_acc, valid_acc, test_acc);
				
				accs.add(t_acc);
				
			}
			
			dt_obj.print_matlab(accs);
			
		}catch(Exception e){
			
			System.out.println(e.toString());
			
		}
		
	}
	
	public Node grow_tree(Vector<Vector<Integer>> data, int depth, boolean[] allowed){

        //System.out.println("data size = " + data.size());
		//base case
		if(depth==0){
			return (new Node(get_max_class(data), 0, null, null));
		}
		
		int cover_type = (data.get(0)).get(54); 
		boolean is_same = true;
		for(int i=1; i<data.size(); i++){
			
			if(cover_type!=(data.get(i)).get(54)){
				is_same = false;
				break;
			}
		}
		
		if(is_same){
            //System.out.println("I am good");
			return (new Node (cover_type, 0, null, null));
		}
		
		double median = 0;
		int best_attr = best_attr_split(data,allowed);
		Vector<Vector<Integer>> data0 = new Vector<Vector<Integer>>();
		Vector<Vector<Integer>> data1 = new Vector<Vector<Integer>>();
		
		if(is_discrete[best_attr]){
			
			for(int i=0; i<data.size(); i++){
				
				if((data.get(i)).get(best_attr)==0){
					data0.add(data.get(i));
				}else{
					data1.add(data.get(i));
				}
				
			}
			
		}else{
			
			median = get_median(data, best_attr);
			
			for(int i=0; i<data.size(); i++){
				
				if((data.get(i)).get(best_attr)<=median){
					data0.add(data.get(i));
				}else{
					data1.add(data.get(i));
				}
				
			}
			
		}
		
		boolean[] allowed0 = new boolean[allowed.length];
		boolean[] allowed1 = new boolean[allowed.length];
		
		for(int i=0; i<allowed.length; i++){
			allowed0[i] = allowed[i];
			allowed1[i] = allowed[i];
		}
		
		allowed0[best_attr] = false;
		allowed1[best_attr] = false;
		
		if(data0.size()==0){
			//System.out.println("I am bad");
			//return (new Node(get_max_class(data1), 0, null, null));
			return (new Node(best_attr, median, new Node (get_max_class(data),0,null,null), grow_tree(data1,depth-1,allowed1)));
		}else if(data1.size()==0){
			//System.out.println("I am bad");
			//return (new Node(get_max_class(data0), 0, null, null));
			return (new Node(best_attr, median, grow_tree(data0,depth-1,allowed0),new Node (get_max_class(data),0,null,null)));
		}
		
		
		
		
		
		return (new Node(best_attr, median, grow_tree(data0,depth-1,allowed0), grow_tree(data1,depth-1,allowed1)));
		
	}
	
	public int best_attr_split(Vector<Vector<Integer>> data, boolean[] allowed){
		
		double h_of_y = 0;
	    int[] class_freq = new int[8];
		
		for(int i=0; i<data.size(); i++){
			
			class_freq[(data.get(i)).get(54)]++;
			
		}
		
		for(int i=1; i<=7; i++){
			if(class_freq[i]!=0){
				double p_of_y = (double) (class_freq[i])/(data.size()); //prior probabilities
				h_of_y += (p_of_y)*(-1)*Math.log(p_of_y);				
			}

		}
		
		//System.out.println(h_of_y);
		
		double mi = Integer.MIN_VALUE;
		int best_attr = 0;
		
		for(int i=0; i<54; i++){
			
			if(is_discrete[i]){
				
				if(!allowed[i]){
					continue;
				}
				
				int num0 = 0;
				
				double h_of_y_given0 = 0;
				int[] class_freq0 = new int[8];
				double h_of_y_given1 = 0;
				int[] class_freq1 = new int[8];
				
				for(int j=0; j<data.size(); j++){
					
					if((data.get(j)).get(i)==0){
						num0++;
						class_freq0[(data.get(j)).get(54)]++;
					}else{
						class_freq1[(data.get(j)).get(54)]++;
					}
					
				}
				
				for(int j=1; j<=7; j++){
					
					if(class_freq0[j]!=0){
						double p_of_y = (double) (class_freq0[j])/(num0);
						h_of_y_given0 += (p_of_y)*(-1)*Math.log(p_of_y);						
					}

					if(class_freq1[j]!=0){
						double p_of_y = (double) (class_freq1[j])/(data.size()-num0);
						h_of_y_given1 += (p_of_y)*(-1)*Math.log(p_of_y);						
					}

					
				}				

				double h_of_y_given_x = ((double) (num0)/(data.size()))*h_of_y_given0 + ((double) (data.size()-num0)/(data.size()))*h_of_y_given1;
				//System.out.println(h_of_y - h_of_y_given_x);
				
				if(mi < h_of_y - h_of_y_given_x){
					mi = h_of_y - h_of_y_given_x;
					best_attr = i;
				}
				
			}else{
				
				double median = get_median(data, i);
				
				int num0 = 0;
				
				double h_of_y_given0 = 0;
				int[] class_freq0 = new int[8];
				double h_of_y_given1 = 0;
				int[] class_freq1 = new int[8];
				
				for(int j=0; j<data.size(); j++){
					
					if((data.get(j)).get(i)<=median){
						num0++;
						class_freq0[(data.get(j)).get(54)]++;
					}else{
						class_freq1[(data.get(j)).get(54)]++;
					}
					
				}
				
				for(int j=1; j<=7; j++){
					if(class_freq0[j]!=0){
						double p_of_y = (double) (class_freq0[j])/(num0);
						h_of_y_given0 += (p_of_y)*(-1)*Math.log(p_of_y);						
					}

					if(class_freq1[j]!=0){
						double p_of_y = (double) (class_freq1[j])/(data.size()-num0);
						h_of_y_given1 += (p_of_y)*(-1)*Math.log(p_of_y);						
					}
				}				
				
				double h_of_y_given_x = ((double) (num0)/(data.size()))*h_of_y_given0 + ((double) (data.size()-num0)/(data.size()))*h_of_y_given1;
				
				//System.out.println(h_of_y - h_of_y_given_x);
				
				if(mi < h_of_y - h_of_y_given_x){
					mi = h_of_y - h_of_y_given_x;
					best_attr = i;
				}
				
			}
			
		}
		
		
		return best_attr;
		
	}
	
	public double get_median(Vector<Vector<Integer>> data, int attr){
		
		int[] arr = new int[data.size()];
		
		for(int i=0; i<data.size(); i++){
			arr[i] = (data.get(i)).get(attr);
		}
		
		Arrays.sort(arr);
		
		if (arr.length%2==0){
            return (double)(arr[arr.length/2] + arr[arr.length/2 - 1])/2;
        }else{
            return arr[arr.length/2];
	    }
	    
	}
	
	public int get_max_class(Vector<Vector<Integer>> data){
		
		int[] class_freq = new int[8];
		
		for(int i=0; i<data.size(); i++){
			class_freq[(data.get(i)).get(54)]++;
		}
		
		int max = 0;
		int max_class = 0;
		
		for(int i=1; i<=7; i++){
			if(max<class_freq[i]){
				max_class = i;
				max = class_freq[i];
			}
		}
		
		return max_class;
		
	}
	
	public double predict(Vector<Vector<Integer>> data, Node root){
		
		double success = 0;
		
		for(int i=0; i<data.size(); i++){
			if(get_leaf(data.get(i), root)==data.get(i).get(54)){
				success++;
			}
		}
		
		return success/data.size();
		
	}
	
	public int get_leaf(Vector<Integer> row, Node root){
		
		if(root.left_child!=null && root.right_child!=null){
			
			if(root.median==0){
				if(row.get(root.value)==0){
					return get_leaf(row, root.left_child);
				}else{
					return get_leaf(row, root.right_child);
				}
			}else{
				if(row.get(root.value)<=root.median){
					return get_leaf(row, root.left_child);
				}else{
					return get_leaf(row, root.right_child);
				}
			}
			
		}else{
			
			return root.value;
			
		}
		
	}
	
	public void print_matlab(Vector<tuple> accs){
		
		String train_acc = "";
		String valid_acc = "";
		String test_acc = "";
		
		for(int i=0; i<accs.size(); i++){
			train_acc += accs.get(i).train_acc;
			valid_acc += accs.get(i).valid_acc;
			test_acc += accs.get(i).test_acc;
			train_acc += ";";
			valid_acc += ";";
			test_acc += ";";
		}
		
		System.out.println("train_acc array : " + train_acc);
		System.out.println("valid_acc array : " + valid_acc);
		System.out.println("test_acc array : " + test_acc);
	}
	
}
