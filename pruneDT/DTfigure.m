all_data = importdata('hello.txt');

train_acc = all_data(1,:);
valid_acc = all_data(2,:);
test_acc = all_data(3,:);

train_acc = train_acc*100;
valid_acc = valid_acc*100;
test_acc = test_acc*100;

x = linspace(45490,38803,length(train_acc));

figure();
plot(x,train_acc);
hold on;
plot(x,valid_acc);
hold on;
plot(x,test_acc);
legend('train accuracy','validation accuracy','test accuracy');
xlabel('No. of nodes in tree');
ylabel('Accuracy');
