import numpy
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt

train_data = numpy.loadtxt('train.dat',skiprows=1, delimiter=',', dtype=int)
print("traindata imported")
valid_data = numpy.loadtxt('valid.dat',skiprows=1, delimiter=',', dtype=int)
print("valid_data imported")
test_data = numpy.loadtxt('test.dat',skiprows=1, delimiter=',', dtype=int)
print("test_data imported")
train_data_x = train_data[:, :54];
train_data_y = train_data[:, 54];
valid_data_x = valid_data[:, :54];
valid_data_y = valid_data[:, 54];
test_data_x = test_data[:, :54];
test_data_y = test_data[:, 54];


clf = DecisionTreeClassifier(max_depth=None, min_samples_split=2, min_samples_leaf=1)
clf.fit(train_data_x, train_data_y);
test_acc = (clf.score(test_data_x,test_data_y)); 
train_acc = (clf.score(train_data_x,train_data_y)); 
valid_acc = (clf.score(valid_data_x,valid_data_y)); 
print(test_acc)
print(train_acc)
print(valid_acc)
