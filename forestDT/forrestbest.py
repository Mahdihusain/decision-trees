import numpy
from sklearn.ensemble import RandomForestClassifier

train_data = numpy.loadtxt('train.dat',skiprows=1, delimiter=',', dtype=int)
print("traindata imported")
valid_data = numpy.loadtxt('valid.dat',skiprows=1, delimiter=',', dtype=int)
print("valid_data imported")
test_data = numpy.loadtxt('test.dat',skiprows=1, delimiter=',', dtype=int)
print("test_data imported")
train_data_x = train_data[:, :54];
train_data_y = train_data[:, 54];
valid_data_x = valid_data[:, :54];
valid_data_y = valid_data[:, 54];
test_data_x = test_data[:, :54];
test_data_y = test_data[:, 54];


clf = RandomForestClassifier(bootstrap=False,n_estimators=20,max_features=30)
clf.fit(train_data_x, train_data_y);

print("Validation Accuracy :") 
print(clf.score(valid_data_x,valid_data_y));
print("Train Accuracy :");
print(clf.score(train_data_x,train_data_y));
print("Test Accuracy :");
print(clf.score(test_data_x,test_data_y));

