import numpy
from sklearn.ensemble import RandomForestClassifier

train_data = numpy.loadtxt('train.dat',skiprows=1, delimiter=',', dtype=int)
print("traindata imported")
valid_data = numpy.loadtxt('valid.dat',skiprows=1, delimiter=',', dtype=int)
print("valid_data imported")
test_data = numpy.loadtxt('test.dat',skiprows=1, delimiter=',', dtype=int)
print("test_data imported")
train_data_x = train_data[:, :54];
train_data_y = train_data[:, 54];
valid_data_x = valid_data[:, :54];
valid_data_y = valid_data[:, 54];
test_data_x = test_data[:, :54];
test_data_y = test_data[:, 54];


clf = RandomForestClassifier(bootstrap=True)
clf.fit(train_data_x, train_data_y);

print("valid bs true ") 
print(clf.score(valid_data_x,valid_data_y));
print("train bs true ");
print(clf.score(train_data_x,train_data_y));
print("test bs true ");
print(clf.score(test_data_x,test_data_y));

clf = RandomForestClassifier(bootstrap=False)
clf.fit(train_data_x, train_data_y);

print("valid bs false ")
print(clf.score(valid_data_x,valid_data_y));
print("train bs false ");
print(clf.score(train_data_x,train_data_y));
print("test bs false ");
print(clf.score(test_data_x,test_data_y));
