import numpy
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt


train_data = numpy.loadtxt('train.dat',skiprows=1, delimiter=',', dtype=int)
print("traindata imported")
valid_data = numpy.loadtxt('valid.dat',skiprows=1, delimiter=',', dtype=int)
print("valid_data imported")
test_data = numpy.loadtxt('test.dat',skiprows=1, delimiter=',', dtype=int)
print("test_data imported")
train_data_x = train_data[:, :54];
train_data_y = train_data[:, 54];
valid_data_x = valid_data[:, :54];
valid_data_y = valid_data[:, 54];
test_data_x = test_data[:, :54];
test_data_y = test_data[:, 54];


valid_accs = []
train_accs = []
test_accs = []
split = []

for num in range(1,20):
    clf = RandomForestClassifier(n_estimators=num)
    clf.fit(train_data_x, train_data_y);
    valid_accs.append(clf.score(valid_data_x,valid_data_y)); 
    train_accs.append(clf.score(train_data_x,train_data_y));
    test_accs.append(clf.score(test_data_x,test_data_y));
    split.append(num) 

train_h = plt.plot(split,train_accs, label='train accuracy')
valid_h = plt.plot(split,valid_accs, label='validation accuracy')
test_h = plt.plot(split,test_accs, label = 'test accuracy')
plt.xlabel('n estimator');
plt.ylabel('Accuracy');
plt.legend([train_h, valid_h, test_h], ['train accuracy','validation accuracy','test accuracy'])
plt.show()
